#!/bin/bash
USER="{{ cookiecutter.username }}"
EMAIL="{{ cookiecutter.email }}"
if [ -f /usr/bin/virtualenv ]
then
  virtualenv -p {{ cookiecutter.python }} venv
else
  {{cookiecutter.python}} -m venv venv
fi
source venv/bin/activate
cd back
echo "Installing backend dependencies"
pip install --upgrade pip setuptools
pip install -r requirements.txt
pip freeze --local > requirements.text
cp environ.py.dist environ.py
python manage.py migrate --noinput
{% if cookiecutter.create_superuser == 'yes' %}
  echo "Creating superuper {{ cookiecutter.username }}"
  python manage.py createsuperuser --username {{ cookiecutter.username }} --email {{ cookiecutter.email }}
{% endif %}
deactivate
cd ..
echo "Initializing new Ember application"
ember new {{ cookiecutter.module_prefix }}
mv {{ cookiecutter.module_prefix }} front
cd front
echo "Installing frontend depencencies"
npm install && bower install
ember install ember-component-css ember-django-adapter ember-simple-auth ember-cli-js-cookie liquid-fire ember-legacy-views smoke-and-mirrors ember-cli-mirage ember-data-factory-guy sl-ember-test-helpers broccoli-serviceworker
npm install --save-dev ember-load ember-cli-sass
cd ..
echo "Downloading vendor libraries"
wget https://github.com/twbs/bootstrap-sass/archive/v{{ cookiecutter.bootstrap_version }}.tar.gz
tar -xzf v{{ cookiecutter.bootstrap_version }}.tar.gz
mkdir front/vendor/bootstrap
mv bootstrap-sass-{{ cookiecutter.bootstrap_version }}/assets/javascripts/* front/vendor/bootstrap/
mkdir front/public/assets
mv bootstrap-sass-{{ cookiecutter.bootstrap_version }}/assets/fonts front/public/assets/
mv bootstrap-sass-{{ cookiecutter.bootstrap_version }}/assets/stylesheets/* front/app/styles/
rm v{{ cookiecutter.bootstrap_version }}.tar.gz
rm -rf bootstrap-sass-{{ cookiecutter.bootstrap_version }}
wget https://github.com/FortAwesome/Font-Awesome/archive/v{{ cookiecutter.fontawesome_version }}.tar.gz
tar -xzf v{{ cookiecutter.fontawesome_version }}.tar.gz
mkdir front/app/styles/fontawesome
mv font-awesome-{{ cookiecutter.fontawesome_version }}/scss/_* front/app/styles/fontawesome/
mv font-awesome-{{ cookiecutter.fontawesome_version }}/fonts front/public/assets/fonts/fontawesome
rm font-awesome-{{ cookiecutter.fontawesome_version }}.tar.gz
rm -rf font-awesome-{{ cookiecutter.fontawesome_version }}
echo "Binding frontend with backend"
rm front/app/templates/application.hbs
rm front/app/styles/app.css
rm -rf front/.git
cp -r front_tmp/* front/
rm -rf front_tmp
cd front
ember build -prod
cd ..
source venv/bin/activate
cd back
mkdir {{ cookiecutter.repo_name }}/templates
cd {{ cookiecutter.repo_name }}/templates
ln -s ../../../front/dist/index.html .
cd ..
ln -s ../../front/dist/assets static
cd ..
python manage.py collectstatic --noinput
cd ..
mv gitignore.txt .gitignore
git init .
git add .
git commit -m "{{ cookiecutter.project_name }} initialized with cookiecutter from http://bitbucket.org/levit_scs/cc_django_ember_app"
