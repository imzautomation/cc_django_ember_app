#!/bin/bash

if [ ! -d "../venv/"  ]; then
  virtualenv -p {{cookiecutter.python}} ../venv
  pip install -U pip setuptools
fi

if [ ! -f "environ.py"  ]; then
  cp environ.py.dist environ.py
fi

source ../venv/bin/activate

pip install -r requirements.txt

./manage.py migrate
./manage.py runserver 0.0.0.0:8000
