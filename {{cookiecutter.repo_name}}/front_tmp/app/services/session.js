import Ember from 'ember';
import config from '../config/environment';
import SessionService from 'ember-simple-auth/services/session';

const { observer } = Ember;

export default SessionService.extend({
  currentUser: null,
  loginError: false,
  setCurrentUser: observer('isAuthenticated', function() {
    if (this.get('isAuthenticated')) {
      const globalConfig = config['ember-simple-auth'] || {};
      const serverAuthEndpoint = globalConfig.serverAuthEndpoint || '/rest-auth';
      Ember.$.ajax(`${serverAuthEndpoint}/me/`, {
        dataType: "json",
        success: (json) => {
          this.set('currentUser', json);
        }
      });
    } else {
      this.set('currentUser', '');
    }
  })
});

